# README #

This is the beginnings of a styleguide for NorthPoint Digital. It is loosely based off of http://northpointdigital.com but may be used for any other project.

### What is this repository for? ###
* Use this stylesheet as a guideline for all new projects. It will ensure that all developers who work on this project are on the same page in terms of styling, as well as provide a guide for designers to know which elements they need to design.

### Contribution guidelines ###

* Please add any markup you think will be helpful
* Markup should follow this format: "<div class="display">[Markup being added]</div>". It will automatically generate the textarea that displays the code.

### Who do I talk to? ###

* This styleguide was created by S. Kimball with contributions by Erin Regan.