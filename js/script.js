// jQuery document ready
$( document ).ready(function() {
	$( ".display" ).each(function() {
		var elementHTML = $(this).html().replace(/\t/g, '&nbsp;&nbsp;&nbsp;&nbsp;');
		$(this).after("<form><textarea cols='30' rows='6' class='markup'>" + $.trim(elementHTML) + "</textarea></form>");
	});
});